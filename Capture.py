#Python exercise - learning to capture packets to a pcap file, then read from them.

from scapy.all import *

print("Let's do this!")

def display(packet):
	print(packet.show())

packets = sniff(count=5, prn=display)
wrpcap('captured.pcap', packets)

#reader = rdpcap("captured.pcap")
#print(reader)