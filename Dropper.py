import os

### A quick script which drops packets going from an ipcamera to a client. ###
iprule = "sudo iptables -I FORWARD -s 192.168.1.2  -d 192.168.1.4 -j DROP"
os.system(iprule)

print("[*] Rule created in iptables...")



### Deletes the first rule in the table - our rule. ###
### Take care when using this if existing rules are in place! ###
#iprestore="iptables -D -I I1"
## "sudo iptables -F" to  flush all rules - dangerous but effective.
#os.system(iprestore)
