#!/usr/bin/python3
import http.server
import socketserver

ADDR = "127.0.0.1"
PORT = 9005
print("[*] Setting up a quick HTTP Server at", ADDR)
Handler = http.server.SimpleHTTPRequestHandler
print("[*] Port and Handler set.")

#Empty TCPServer IP Address "" results in listening on any and all network interfaces.
with socketserver.TCPServer((ADDR, PORT), Handler) as httpd:
    print("[*] Serving on port", PORT)
    httpd.serve_forever()

