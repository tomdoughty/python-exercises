from scapy.all import *
from netfilterqueue import NetfilterQueue
import os

### LINKS ###
## https://github.com/kti/python-netfilterqueue
## https://gist.github.com/eXenon/85a3eab09fefbb3bee5d
## https://ritcsec.wordpress.com/2016/12/11/utilizing-nfqueue-scapy-and-http-for-persistence/

p = sniff(count=150) #, filter="192.168.1.2")

for pkt in p:
	if UDP in pkt:
		del pkt.chksum
		print("Checksum removed.")

def modify(packet):
	packet.drop()
	send(p)
	print("Sending packets...")




def inspect(packet):
	#print("[*] Inspecting packet contents...")
	pkt = packet.get_payload()
	print(pkt)
	packet.accept()

print("[*] Setting up iptables rule.")
print("[*] When removing this rule from iptables it is assumed no other rules are in place!")

#switched from INPUT to FORWARD
#QueueRule = "sudo iptables -I FORWARD -s 192.168.1.2 -d 192.168.1.3 -j NFQUEUE --queue-num 1"
QueueRule = "sudo iptables -I INPUT -s 192.168.1.2 -d 192.168.1.3 -j NFQUEUE --queue-num 1"

os.system(QueueRule)

nfq = NetfilterQueue()
nfq.bind(1, modify)

try:

	nfq.run()
except KeyboardInterrupt:
	pass


#nfq.unbind()


#iprule = "sudo iptables -I INPUT -p udp -j NFQUEUE --queue-num 1"
#iprule = "sudo iptables -I INPUT -s 192.168.1.2 -j DROP"

#p = sniff(count=150, filter="host 192.168.1.2 and udp") 


#iprestore="iptables -D -I INPUT 1"
#-s 192.168.1.2 -j DROP"
#iprestore="iptables -D -t nat -A PREROUTING -p udp -j NFQUEUE --queue-num 1"


### OLD MODIFY FUNCTION ###
#def modify(packet):
	#pkt = IP(packet.get_payload())
	#pkt[UDP].payload=reader[UDP].payload
	#forged_payload=reader[UDP].payload
	#pkt[UDP].chksum=0
	#pkt[UDP].chksum=0
	#print(pkt)
	#packet.set_payload(str(forged_payload))
#	packet.drop()

#	forgery=IP() /UDP() /RTP()
#	forgery.src="192.168.1.2"
#	forgery.dst="192.168.1.3"
	#forgery.sport=reader[UDP].sport
	#forgery.dport=reader[UDP].dport
	#forgery=reader[UDP].chksum= 0
	#forgery=reader[IP].chksum = 0
	#forgery=reader[UDP].payload
